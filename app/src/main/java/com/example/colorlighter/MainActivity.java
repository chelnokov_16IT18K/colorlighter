package com.example.colorlighter;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button red = findViewById(R.id.red);
        Button yellow = findViewById(R.id.yellow);
        Button green = findViewById(R.id.green);
        setContentView(R.layout.activity_main);
    }
    public void onRed(View view) {
        LinearLayout linearLayout = findViewById(R.id.container);
        linearLayout.setBackgroundColor(Color.RED);
    }

    public void onYellow(View view) {
        LinearLayout linearLayout = findViewById(R.id.container);
        linearLayout.setBackgroundColor(Color.YELLOW);
    }

    public void onGreen(View view) {
        LinearLayout linearLayout = findViewById(R.id.container);
        linearLayout.setBackgroundColor(Color.GREEN);
    }
}
